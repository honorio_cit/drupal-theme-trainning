(function ($) {

  Drupal.behaviors.ninesixtyrobots = {
    attach: function(context) {
      $(document).ready(function() {
        $('#search-block-form input:text').autofill({
          value: "Search..."
        });
      })

    }
  };

})(jQuery);
