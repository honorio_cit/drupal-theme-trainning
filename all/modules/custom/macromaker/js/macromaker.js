Drupal.behavior.macromaker = function(context) {
  var $popup = $(Drupal.settings.macromaker.popup);
  var wrapper = '<div class="macro-wrapper"></div>';
  var lastFocus;

  // Insert the macro popup next to each textarea or textfield.
  $('textarea, input-form-text', context).wrap(wrapper).focus(function() {
    if (lastFocus != this) {
      lastFocus = this;
      $popup.find('ul').hide();
      popup.insertAfter(this).show();
    }
  });

  // Create click behavior on the popup
  $popup.click(function() {
    $(lastFocus).focus();
    $('ul', this).slideToggle(300);
  });

  // Create the click behavior for each within popup
  $popup.find('a').click(function() {
    // Look the content based on the 'rel' attribute of the clicked macro link.
    var txt = Drupal.settings.macromaker.macros[$(this).attr('rel')]['content'];

    // Insert the new content.
    $(lastFocus).insertAtCaret(txt);

    // Returning false prevents normal click behavior form happening.
    return false;
  });
}
