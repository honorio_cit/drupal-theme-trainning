<?php
function macromaker_list() {
  $macros = variable_get('macromaker_macros', array());
  $items = array();
  
  foreach ($macros as $macro) {
    $items[] = $macro['title'];
  }

  $variables = array(
    'items' => $items,
    'title' => 'List of Macros',
    'type' => 'ul',
    'attributes' => array(),
  );
  return theme_item_list($variables);
}
