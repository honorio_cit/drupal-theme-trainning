<?php
function macromaker_edit_form($form, &$form_state) {

  $form = array();
  $form['mid'] = array(
    '#type'  => 'textfield',
    '#title' => 'Macro ID',
    '#size'  => 5,
  );
  $form['title'] = array(
    '#type'  => 'textfield',
    '#title' => 'Macro\'s name',
    '#size'  => 50,
  );
  $form['content'] = array(
    '#type'  => 'textarea',
    '#title' => 'Macro\'s content',
  );

  $form['buttons'] = array();
  $form['buttons']['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Submit',
    '#submit' => array('macromaker_edit_form_submit'),
  );
  $form['buttons']['load'] = array(
    '#type'   => 'submit',
    '#value'  => 'Load',
    '#submit' => array('macromaker_edit_form_submit_load'),
  );

  return $form;
}

function macromaker_edit_form_validate($form, &$form_state) {
  if (!is_numeric($form_state['values']['mid'])) {
    form_set_error('mid',t('Macro\'s ID must be a number'));
  }
}

function macromaker_edit_form_submit($form, &$form_state) {
  $form_state['redirect'] = '/macros'; 
}

function macromaker_edit_form_submit_load($form, &$form_state) {
  $macros = variable_get('macromaker_macros', array());

  $mid = $form_state['values']['mid'];

  $index = array_search($mid, array_column($macros, 'mid'));

  dsm($macros);
  dsm($mid);

  if ($index === false) {
    //form_set_error('mid', '\'mid\' = @mid do not exist', array('@mid' => $mid));
    $form_state['redirect'] = '/macros/edit';
  }
  else {
    dsm($index);
    $form_state['redirect'] = "/macros/edit?mid=$mid";
  }

}

