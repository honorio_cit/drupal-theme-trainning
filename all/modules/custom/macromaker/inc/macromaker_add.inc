<?php
function macromaker_add_form($form, &$form_state) {

  $form = array();
  $form['new_macro'] = array(
    '#type'  => 'fieldset',
    '#title' => 'Add macro',
    '#collapsible' => true,
  );
  $form['new_macro']['mid'] = array(
    '#type'  => 'textfield',
    '#title' => 'Macro ID',
    '#size'  => 5,
  );
  $form['new_macro']['title'] = array(
    '#type'  => 'textfield',
    '#title' => 'Macro\'s name',
    '#size'  => 50,
    '#autocomplete_path' => 'macros/name_autocomplete'
  );
  $form['new_macro']['content'] = array(
    '#type'  => 'textarea',
    '#title' => 'Macro\'s content',
  );
  $form['new_macro']['submit'] = array(
    '#type'  => 'submit',
    '#value' => 'Submit',
  );

  return $form;
}

function macromaker_add_form_validate($form , &$form_state) {
  if (!is_numeric($form_state['values']['mid'])) {
    form_set_error('mid',t('Macro\'s ID must be a number'));
  }
}
  
function macromaker_add_form_submit($form, &$form_state) {
  $macros   = variable_get('macromaker_macros', array());
  $mid      = $form_state['values']['mid'];
  $title    = $form_state['values']['title'];
  $content  = $form_state['values']['content'];
  $macros[] = array('mid' => $mid, 'title' => $title, 'content' => $content);
  variable_set('macromaker_macros', $macros);
  $form_state['redirect'] = '/macros'; 
}

function name_autocomplete($str =  '') {
  $macros = variable_get('macromaker_macros', array());
  $matches = array();
  foreach ($macros as $macro) {
    if (strstr($macro['title'], $str)) {
      $matches[] = $macro['title'];
    }
  }

  return drupal_json_output($matches);
}
